# DKX/K8S Factories Generator

Generator for kubernetes factories

## Installation

```bash
$ npm install --save-dev @dkx/k8s-factories-generator
```

## Usage

```bash
$ npx k8s-factories-generator gen release-1.15 out/v1.15
```
