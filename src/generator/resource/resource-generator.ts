import * as path from 'path';
import * as fs from 'fs-extra';
import * as ts from 'typescript';

import {addJSDoc, createNamedImports, NamedImport, printTypescript} from '../../utils';
import {Definition, Document, Manager} from '../../openapi';


export async function generateResources(typesManager: Manager, doc: Document): Promise<void>
{
	process.stdout.write('Generating resources...');

	const definitions = doc.definitions.filter((definition) => {
		return !definition.shouldBeSkipped;
	});

	for (let definition of definitions) {
		await generateResource(typesManager, doc, definition);
	}

	console.log(' done');
}

async function generateResource(typesManager: Manager, doc: Document, definition: Definition): Promise<void>
{
	const statements: Array<ts.Statement> = [];

	if (definition.properties.length > 0) {
		statements.push(generateResourceOptions(typesManager, doc, definition));
	}

	statements.push(generateResourceModel(typesManager, definition));

	const code = printTypescript(definition.filename, [
		...generateResourceImports(doc, definition),
		...statements,
	]);

	await fs.mkdirp(path.dirname(definition.filename));
	await fs.writeFile(definition.filename, code, {encoding: 'utf8'});
}

function generateResourceImports(doc: Document, definition: Definition): Array<ts.ImportDeclaration>
{
	return createNamedImports([
		{
			source: definition.filename,
			target: path.join(doc.resourceDir, 'resource-interface.ts'),
			names: ['Resource'],
		},
		...definition.getDependenciesNames()
			.map((name) => {
				return doc.getDefinition(name);
			})
			.filter((dependency) => {
				return !dependency.shouldBeSkipped;
			})
			.map((dependency): NamedImport => {
				return {
					source: definition.filename,
					target: dependency.filename,
					names: [dependency.modelName, dependency.optionsName],
				};
			}),
	]);
}

function generateResourceOptions(typesManager: Manager, doc: Document, definition: Definition): ts.InterfaceDeclaration
{
	return ts.createInterfaceDeclaration(
		[],
		[
			ts.createToken(ts.SyntaxKind.ExportKeyword),
			ts.createToken(ts.SyntaxKind.DeclareKeyword),
		],
		ts.createIdentifier(definition.optionsName),
		[],
		[],
		definition.sortedProperties
			.filter((property) => property.inOptions)
			.map((property): ts.PropertySignature => {
				const questionToken: ts.QuestionToken|undefined = property.required ?
					undefined :
					ts.createToken(ts.SyntaxKind.QuestionToken);

				return ts.createPropertySignature(
					[],
					ts.createStringLiteral(property.name),
					questionToken,
					typesManager.createOptionType(property),
					undefined,
				);
			}),
	);
}

function generateResourceModel(typesManager: Manager, definition: Definition): ts.ClassDeclaration
{
	const members: Array<ts.ClassElement> = definition.sortedProperties
		.map((property) => {
			const questionToken: ts.QuestionToken|undefined = property.required ?
				undefined :
				ts.createToken(ts.SyntaxKind.QuestionToken);

			const prop = ts.createProperty(
				[],
				[
					ts.createToken(ts.SyntaxKind.PublicKeyword),
					ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
				],
				ts.createStringLiteral(property.name),
				questionToken,
				typesManager.createPropertyType(property),
				property.value,
			);

			const description = property.description;
			if (typeof description !== 'undefined') {
				return addJSDoc(prop, [description]);
			}

			return prop;
		});

	if (definition.properties.length > 0) {
		members.push(createConstructor(typesManager, definition));
	}

	members.push(createJsonFactoryMethod(typesManager, definition));
	members.push(createToJsonMethod(typesManager, definition));

	const model = ts.createClassDeclaration(
		[],
		[ts.createToken(ts.SyntaxKind.ExportKeyword)],
		ts.createIdentifier(definition.modelName),
		[],
		[
			ts.createHeritageClause(
				ts.SyntaxKind.ImplementsKeyword,
				[
					ts.createExpressionWithTypeArguments(
						[],
						ts.createIdentifier('Resource'),
					),
				],
			),
		],
		members,
	);

	const description = definition.description;
	if (typeof description !== 'undefined') {
		const jsDoc: Array<string> = [description];
		if (definition.isDeprecated()) {
			jsDoc.push('');
			jsDoc.push('@deprecated');
		}

		return addJSDoc(model, jsDoc);
	}

	return model;
}

function createConstructor(typesManager: Manager, definition: Definition): ts.ConstructorDeclaration
{
	const assignments: Array<ts.Statement> = [];
	let optionsRequired = false;

	const properties = definition.sortedProperties.filter((property) => typeof property.value === 'undefined');

	properties
		.filter((property) => property.required)
		.forEach((property) => {
			optionsRequired = true;
			assignments.push(ts.createExpressionStatement(
				ts.createBinary(
					ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name)),
					ts.createToken(ts.SyntaxKind.EqualsToken),
					typesManager.createFromOptionsExpression(
						property,
						ts.createElementAccess(ts.createIdentifier('options'), ts.createStringLiteral(property.name)),
					),
				),
			));
		});

	properties
		.filter((property) => !property.required)
		.forEach((property) => {
			assignments.push(ts.createIf(
				ts.createBinary(
					ts.createTypeOf(
						ts.createElementAccess(ts.createIdentifier('options'), ts.createStringLiteral(property.name)),
					),
					ts.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createBinary(
							ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name)),
							ts.createToken(ts.SyntaxKind.EqualsToken),
							typesManager.createFromOptionsExpression(
								property,
								ts.createElementAccess(ts.createIdentifier('options'), ts.createStringLiteral(property.name)),
							),
						),
					),
				]),
			));
		});

	return ts.createConstructor(
		[],
		[],
		[ts.createParameter(
			[],
			[],
			undefined,
			ts.createIdentifier('options'),
			undefined,
			ts.createTypeReferenceNode(ts.createIdentifier(definition.optionsName), []),
			optionsRequired ? undefined : ts.createObjectLiteral([], false),
		)],
		ts.createBlock(assignments, true),
	);
}

function createJsonFactoryMethod(typesManager: Manager, definition: Definition): ts.MethodDeclaration
{
	const required = definition.sortedProperties.filter((property) => property.required && typeof property.value === 'undefined');
	const optional = definition.sortedProperties.filter((property) => !property.required);

	const statements: Array<ts.Statement> = [];
	const ctorArguments: Array<ts.Expression> = [];
	const factoryParameters: Array<ts.ParameterDeclaration> = [];

	if (required.length + optional.length > 0) {
		required.forEach((property) => {
			statements.push(ts.createIf(
				ts.createBinary(
					ts.createTypeOf(ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.apiName))),
					ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createBlock([
					ts.createThrow(
						ts.createNew(
							ts.createIdentifier('Error'),
							[],
							[
								ts.createStringLiteral(`${definition.modelName}.createFromJson: field "${property.apiName}" is required`),
							],
						),
					),
				], true),
			));
		});

		statements.push(ts.createVariableStatement(
			[],
			ts.createVariableDeclarationList([
				ts.createVariableDeclaration(
					ts.createIdentifier('options'),
					ts.createTypeReferenceNode(definition.optionsName, []),
					ts.createObjectLiteral(required.map((property) => {
						return ts.createPropertyAssignment(
							ts.createStringLiteral(property.name),
							typesManager.createFromJsonExpression(
								property,
								ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.apiName)),
							),
						);
					}), true),
				),
			], ts.NodeFlags.Const),
		));

		optional.forEach((property) => {
			statements.push(ts.createIf(
				ts.createBinary(
					ts.createTypeOf(ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.apiName))),
					ts.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createBlock([
					ts.createExpressionStatement(ts.createBinary(
						ts.createElementAccess(ts.createIdentifier('options'), ts.createStringLiteral(property.name)),
						ts.createToken(ts.SyntaxKind.EqualsToken),
						typesManager.createFromJsonExpression(
							property,
							ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.apiName)),
						),
					)),
				], true),
			));
		});

		ctorArguments.push(ts.createIdentifier('options'));

		factoryParameters.push(ts.createParameter(
			[],
			[],
			undefined,
			ts.createIdentifier('data'),
			undefined,
			ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
		));
	}

	statements.push(ts.createReturn(ts.createNew(
		ts.createIdentifier(definition.modelName),
		[],
		ctorArguments,
	)));

	return ts.createMethod(
		[],
		[
			ts.createToken(ts.SyntaxKind.PublicKeyword),
			ts.createToken(ts.SyntaxKind.StaticKeyword),
		],
		undefined,
		ts.createIdentifier('createFromJson'),
		undefined,
		[],
		factoryParameters,
		ts.createTypeReferenceNode(ts.createIdentifier(definition.modelName), []),
		ts.createBlock(statements, true),
	);
}

function createToJsonMethod(typesManager: Manager, definition: Definition): ts.MethodDeclaration
{
	const required = definition.sortedProperties.filter((property) => property.required);
	const optional = definition.sortedProperties.filter((property) => !property.required);

	const initProperties: Array<ts.PropertyAssignment> = required.map((property) => {
		return ts.createPropertyAssignment(
			ts.createStringLiteral(property.apiName),
			typesManager.createToJsonExpression(
				property,
				ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name)),
			),
		);
	});

	const statements: Array<ts.Statement> = [];

	if (optional.length === 0) {
		statements.push(ts.createReturn(ts.createObjectLiteral(initProperties, true)));
	} else {
		statements.push(ts.createVariableStatement(
			[],
			ts.createVariableDeclarationList([
				ts.createVariableDeclaration(
					ts.createIdentifier('data'),
					ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
					ts.createObjectLiteral(initProperties, true),
				),
			], ts.NodeFlags.Const),
		));

		optional.forEach((property) => {
			statements.push(ts.createIf(
				ts.createBinary(
					ts.createTypeOf(
						ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name)),
					),
					ts.createToken(ts.SyntaxKind.ExclamationEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createBinary(
							ts.createElementAccess(ts.createIdentifier('data'), ts.createStringLiteral(property.apiName)),
							ts.createToken(ts.SyntaxKind.EqualsToken),
							typesManager.createToJsonExpression(
								property,
								ts.createElementAccess(ts.createThis(), ts.createStringLiteral(property.name)),
							),
						),
					),
				]),
			));
		});

		statements.push(ts.createReturn(ts.createIdentifier('data')));
	}

	return ts.createMethod(
		[],
		[
			ts.createToken(ts.SyntaxKind.PublicKeyword),
		],
		undefined,
		ts.createIdentifier('toJson'),
		undefined,
		undefined,
		[],
		ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
		ts.createBlock(statements, true),
	);
}
