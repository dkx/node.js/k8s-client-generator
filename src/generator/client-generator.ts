import * as ts from 'typescript';
import * as fs from 'fs-extra';
import * as path from 'path';

import {Document} from '../openapi';
import {printTypescript, createNamedImports, firstLower, NamedImport} from '../utils';


export async function generateClient(doc: Document): Promise<void>
{
	process.stdout.write('Generating api client...');

	const filename = path.join(doc.dir, 'client.ts');
	const code = printTypescript(filename, [
		...createImports(doc, filename),
		createClass(doc),
	]);

	await fs.mkdirp(path.dirname(filename));
	await fs.writeFile(filename, code, {encoding: 'utf8'});

	console.log(' done');
}

function createImports(doc: Document, filename: string): Array<ts.ImportDeclaration>
{
	return createNamedImports([
		{
			source: filename,
			target: path.join(doc.dir, 'fetch.ts'),
			names: ['Fetch'],
		},
		{
			source: filename,
			target: path.join(doc.dir, 'logging', 'index.ts'),
			names: ['Logger', 'StdOutLogger'],
		},
		{
			source: filename,
			target: path.join(doc.dir, 'json-patch-generator.ts'),
			names: ['JsonPatchGenerator'],
		},
		...doc.getRootApis().map((group): NamedImport => {
			return {
				source: filename,
				target: group.filename,
				names: [group.className],
			};
		}),
	]);
}

function createClass(doc: Document): ts.ClassDeclaration
{
	return ts.createClassDeclaration(
		[],
		[
			ts.createToken(ts.SyntaxKind.ExportKeyword),
		],
		ts.createIdentifier('Client'),
		[],
		[],
		[
			ts.createProperty(
				[],
				[
					ts.createToken(ts.SyntaxKind.PrivateKeyword),
					ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
				],
				ts.createIdentifier('jsonPatchGenerator'),
				undefined,
				ts.createTypeReferenceNode(ts.createIdentifier('JsonPatchGenerator'), []),
				undefined,
			),
			ts.createConstructor(
				[],
				[],
				[
					ts.createParameter(
						[],
						[
							ts.createToken(ts.SyntaxKind.PrivateKeyword),
							ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
						],
						undefined,
						ts.createIdentifier('fetch'),
						undefined,
						ts.createTypeReferenceNode(ts.createIdentifier('Fetch'), []),
						undefined,
					),
					ts.createParameter(
						[],
						[
							ts.createToken(ts.SyntaxKind.PrivateKeyword),
							ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
						],
						undefined,
						ts.createIdentifier('logger'),
						undefined,
						ts.createTypeReferenceNode(ts.createIdentifier('Logger'), []),
						ts.createNew(
							ts.createIdentifier('StdOutLogger'),
							[],
							[],
						),
					),
				],
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createBinary(
							ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('jsonPatchGenerator')),
							ts.createToken(ts.SyntaxKind.EqualsToken),
							ts.createNew(
								ts.createIdentifier('JsonPatchGenerator'),
								[],
								[],
							),
						),
					),
				], true),
			),
			...doc.getRootApis().map((group): ts.GetAccessorDeclaration => {
				return ts.createGetAccessor(
					[],
					[
						ts.createToken(ts.SyntaxKind.PublicKeyword),
					],
					ts.createIdentifier(firstLower(group.className.slice(0, -3))),
					[],
					ts.createTypeReferenceNode(ts.createIdentifier(group.className), []),
					ts.createBlock([
						ts.createReturn(
							ts.createNew(
								ts.createIdentifier(group.className),
								[],
								[
									ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('fetch')),
									ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('logger')),
									ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('jsonPatchGenerator')),
								],
							),
						),
					], true),
				);
			}),
		],
	);
}
