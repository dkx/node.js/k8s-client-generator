import * as fs from 'fs-extra';
import * as path from 'path';

import {Document} from '../openapi';


export async function generateStaticContent(doc: Document): Promise<void>
{
	process.stdout.write('Copying static content...');

	await fs.copy(path.join(__dirname, '..', '..', 'static', '.'), path.join(doc.dir, '.'));

	console.log(' done');
}
