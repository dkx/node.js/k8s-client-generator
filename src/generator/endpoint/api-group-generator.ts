import * as path from 'path';
import * as ts from 'typescript';
import * as fs from 'fs-extra';

import {Definition, Document, Endpoint, EndpointFactory, EndpointGroup, EndpointParameterPlace} from '../../openapi';
import {
	addJSDoc,
	createNamedImports,
	definitionToTypeNode, firstUpper,
	getDefinitionToJsonCallback,
	NamedImport,
	NamedImports,
	printTypescript
} from '../../utils';


export async function generateEndpoints(doc: Document): Promise<void>
{
	process.stdout.write('Generating endpoints...');

	for (let group of doc.endpointGroups) {
		await generateFileApiGroup(doc, group);
	}

	console.log(' done');
}

async function generateFileApiGroup(doc: Document, group: EndpointGroup): Promise<void>
{
	const statements: Array<ts.Statement> = [
		...createImports(doc, group),
		createClass(doc, group),
	];

	const code = printTypescript(group.filename, statements);

	await fs.mkdirp(path.dirname(group.filename));
	await fs.writeFile(group.filename, code, {encoding: 'utf8'});
}

function createImports(doc: Document, group: EndpointGroup): Array<ts.ImportDeclaration>
{
	const filename = group.filename;
	const imports: NamedImports = [
		{
			source: filename,
			target: path.join(doc.dir, 'fetch.ts'),
			names: ['Fetch'],
		},
		{
			source: filename,
			target: path.join(doc.dir, 'logging', 'index.ts'),
			names: ['Logger'],
		},
		{
			source: filename,
			target: path.join(doc.dir, 'json-patch-generator.ts'),
			names: ['JsonPatchGenerator'],
		},
		...group.factories.map((factory): NamedImport => {
			return {
				source: filename,
				target: factory.targetEndpointGroup.filename,
				names: [factory.targetEndpointGroup.className],
			};
		}),
		...group.getDependenciesNames()
			.map((name) => doc.getDefinition(name))
			.filter((definition) => {
				return !definition.shouldBeSkipped;
			}).map((definition: Definition): NamedImport => {
				return {
					source: filename,
					target: definition.filename,
					names: [definition.modelName],
				};
			}),
	];

	if (group.shouldCreateExistsShortcut()) {
		imports.push({
			source: filename,
			target: path.join(doc.dir, 'error.ts'),
			names: ['ApiResponseNotFoundError'],
		});
	}

	return createNamedImports(imports);
}

function createClass(doc: Document, group: EndpointGroup): ts.ClassDeclaration
{
	const members: Array<ts.ClassElement> = [
		createConstructor(group),
		...group.factories.map((factory) => createFactoryMethod(factory)),
		...group.endpoints.map((endpoint) => createRequestMethod(doc, group, endpoint)),
	];

	if (group.shouldCreateExistsShortcut()) {
		members.push(createExistsMethod());
	}

	if (group.shouldCreatePostOrShortcut('patch')) {
		members.push(createPostOrMethod(doc, group, 'patch'));
	}

	if (group.shouldCreatePostOrShortcut('put')) {
		members.push(createPostOrMethod(doc, group, 'put'));
	}

	return ts.createClassDeclaration(
		[],
		[ts.createToken(ts.SyntaxKind.ExportKeyword)],
		ts.createIdentifier(group.className),
		[],
		[],
		members,
	);
}

function createConstructor(group: EndpointGroup): ts.ConstructorDeclaration
{
	return addJSDoc(
		ts.createConstructor(
			[],
			[],
			[
				ts.createParameter(
					[],
					[
						ts.createToken(ts.SyntaxKind.PrivateKeyword),
						ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
					],
					undefined,
					ts.createIdentifier('fetch'),
					undefined,
					ts.createTypeReferenceNode(ts.createIdentifier('Fetch'), []),
				),
				ts.createParameter(
					[],
					[
						ts.createToken(ts.SyntaxKind.PrivateKeyword),
						ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
					],
					undefined,
					ts.createIdentifier('logger'),
					undefined,
					ts.createTypeReferenceNode(ts.createIdentifier('Logger'), []),
				),
				ts.createParameter(
					[],
					[
						ts.createToken(ts.SyntaxKind.PrivateKeyword),
						ts.createToken(ts.SyntaxKind.ReadonlyKeyword),
					],
					undefined,
					ts.createIdentifier('jsonPatchGenerator'),
					undefined,
					ts.createTypeReferenceNode(ts.createIdentifier('JsonPatchGenerator'), []),
				),
				...group.ctorParameters.map((parameter): ts.ParameterDeclaration => {
					return ts.createParameter(
						[],
						[
							ts.createToken(ts.SyntaxKind.PrivateKeyword),
						],
						undefined,
						ts.createIdentifier(parameter.name),
						undefined,
						ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
					);
				}),
			],
			ts.createBlock([]),
		), [
			'@param fetch',
			'@param logger',
			'@param jsonPatchGenerator',
			...group.ctorParameters.map((parameter) => {
				return '@param ' + parameter.name + (typeof parameter.description === 'undefined' ? '' : (' - ' + parameter.description));
			}),
		],
	);
}

function createFactoryMethod(factory: EndpointFactory): ts.ClassElement
{
	if (factory.methodParameters.length > 0) {
		return addJSDoc(
			ts.createMethod(
				[],
				[
					ts.createToken(ts.SyntaxKind.PublicKeyword),
				],
				undefined,
				ts.createIdentifier(factory.name),
				undefined,
				[],
				factory.methodParameters.map((parameter) => {
					return ts.createParameter(
						[],
						[],
						undefined,
						ts.createIdentifier(parameter.name),
						undefined,
						ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
						undefined,
					);
				}),
				ts.createTypeReferenceNode(ts.createIdentifier(factory.targetEndpointGroup.className), []),
				ts.createBlock([
					ts.createReturn(
						ts.createNew(
							ts.createIdentifier(factory.targetEndpointGroup.className),
							[],
							[
								ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('fetch')),
								ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('logger')),
								ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('jsonPatchGenerator')),
								...factory.parentParameters.map((parameter) => {
									return ts.createPropertyAccess(ts.createThis(), ts.createIdentifier(parameter.name));
								}),
								...factory.methodParameters.map((parameter) => {
									return ts.createIdentifier(parameter.name);
								}),
							],
						),
					),
				], true),
			), [
				...factory.methodParameters.map((parameter) => {
					return '@param ' + parameter.name + (typeof parameter.description === 'undefined' ? '' : (' - ' + parameter.description));
				}),
			],
		);
	}

	return ts.createGetAccessor(
		[],
		[
			ts.createToken(ts.SyntaxKind.PublicKeyword),
		],
		ts.createIdentifier(factory.name),
		[],
		ts.createTypeReferenceNode(ts.createIdentifier(factory.targetEndpointGroup.className), []),
		ts.createBlock([
			ts.createReturn(
				ts.createNew(
					ts.createIdentifier(factory.targetEndpointGroup.className),
					[],
					[
						ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('fetch')),
						ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('logger')),
						ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('jsonPatchGenerator')),
						...factory.parentParameters.map((parameter) => {
							return ts.createPropertyAccess(ts.createThis(), ts.createIdentifier(parameter.name));
						}),
					],
				),
			),
		], true),
	);
}

function createRequestMethod(doc: Document, group: EndpointGroup, endpoint: Endpoint): ts.MethodDeclaration
{
	const returnRefName = endpoint.getResponseRef(200);
	const returnRef = typeof returnRefName === 'undefined' ? null : doc.getDefinition(returnRefName);

	const parameters: Array<ts.ParameterDeclaration> = [];
	const statements: Array<ts.Statement> = [];
	const comments: Array<string> = [];
	const logData: Array<ts.ObjectLiteralElementLike> = [
		ts.createShorthandPropertyAssignment(ts.createIdentifier('result')),
		ts.createPropertyAssignment(
			ts.createIdentifier('endpoint'),
			ts.createObjectLiteral([
				ts.createPropertyAssignment(ts.createIdentifier('method'), ts.createStringLiteral(endpoint.method.toUpperCase())),
				ts.createPropertyAssignment(ts.createIdentifier('urlPattern'), ts.createStringLiteral(group.url)),
			], true),
		),
	];

	const definitionName = group.findAssociatedDefinition();
	if (typeof definitionName !== 'undefined') {
		const definition = doc.getDefinition(definitionName);
		logData.push(
			ts.createPropertyAssignment(
				ts.createIdentifier('definition'),
				ts.createObjectLiteral([
					ts.createPropertyAssignment(ts.createIdentifier('fullName'), ts.createStringLiteral(definition.fullName)),
					ts.createPropertyAssignment(ts.createIdentifier('className'), ts.createStringLiteral(definition.modelName)),
				], true),
			),
		);
	}

	if (typeof endpoint.description !== 'undefined') {
		comments.push(endpoint.description);
	}

	let isSmartPatch: boolean = false;
	const bodyParameter = endpoint.getParameter(EndpointParameterPlace.Body, 'body');
	if (typeof bodyParameter !== 'undefined') {
		if (typeof bodyParameter.$ref === 'undefined') {
			throw new Error(`Body parameter in ${group.url} is missing a $ref`);
		}

		comments.push('@param resource' + (typeof bodyParameter.description === 'undefined' ? '' : (' - ' + bodyParameter.description)));

		logData.push(ts.createShorthandPropertyAssignment(ts.createIdentifier('resource')));
		logData.push(ts.createShorthandPropertyAssignment(ts.createIdentifier('body')));

		const bodyRef = doc.getDefinition(bodyParameter.$ref);
		let parameterType: ts.TypeNode;

		let assignToBody: ts.Expression = getDefinitionToJsonCallback(bodyRef, ts.createIdentifier('resource'));
		if (!endpoint.isBodyRequired()) {
			assignToBody = ts.createConditional(
				ts.createBinary(
					ts.createTypeOf(ts.createIdentifier('resource')),
					ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
					ts.createStringLiteral('undefined'),
				),
				ts.createToken(ts.SyntaxKind.QuestionToken),
				ts.createIdentifier('undefined'),
				ts.createToken(ts.SyntaxKind.ColonToken),
				assignToBody,
			);
		}

		statements.push(
			ts.createVariableStatement([], ts.createVariableDeclarationList([
				ts.createVariableDeclaration(
					ts.createIdentifier('body'),
					null,
					assignToBody,
				),
			], ts.NodeFlags.Let)),
		);

		if (endpoint.method === 'patch' && bodyRef.fullName === 'io.k8s.apimachinery.pkg.apis.meta.v1.Patch' && returnRef !== null && group.hasEndpoint('get')) {
			isSmartPatch = true;

			parameterType = ts.createUnionTypeNode([
				definitionToTypeNode(returnRef),
				definitionToTypeNode(bodyRef),
			]);

			statements.push(
				ts.createExpressionStatement(
					ts.createBinary(
						ts.createIdentifier('resource'),
						ts.createToken(ts.SyntaxKind.EqualsToken),
						ts.createAwait(
							ts.createCall(
								ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('read')),
								[],
								[],
							),
						),
					),
				),
			);

			statements.push(ts.createIf(
				ts.createBinary(
					ts.createIdentifier('body'),
					ts.createToken(ts.SyntaxKind.InstanceOfKeyword),
					ts.createIdentifier(returnRef.modelName),
				),
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createBinary(
							ts.createIdentifier('body'),
							ts.createToken(ts.SyntaxKind.EqualsToken),
							ts.createCall(
								ts.createPropertyAccess(
									ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('jsonPatchGenerator')),
									ts.createIdentifier('createForResource'),
								),
								[],
								[
									ts.createIdentifier('resource'),
									ts.createIdentifier('body'),
								],
							),
						),
					),
				], true),
				undefined,
			));
		} else {
			parameterType = definitionToTypeNode(bodyRef);
		}

		parameters.push(ts.createParameter(
			[],
			[],
			undefined,
			ts.createIdentifier('resource'),
			endpoint.isBodyRequired() ? undefined : ts.createToken(ts.SyntaxKind.QuestionToken),
			parameterType,
		));
	}

	statements.push(
		ts.createVariableStatement(
			[],
			ts.createVariableDeclarationList([
				ts.createVariableDeclaration(
					ts.createIdentifier('url'),
					undefined,
					group.getUrlAsExpression(),
				),
			], ts.NodeFlags.Const),
		),
	);

	const fetchParams: Array<ts.Expression> = [
		ts.createStringLiteral(endpoint.method.toUpperCase()),
		ts.createIdentifier('url'),
		endpoint.supportsDryRun() ? ts.createTrue() : ts.createFalse(),
	];

	if (typeof bodyParameter !== 'undefined') {
		fetchParams.push(ts.createIdentifier('body'));
	}

	const fetch = ts.createCall(
		ts.createPropertyAccess(
			ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('fetch')),
			ts.createIdentifier('request'),
		),
		[],
		fetchParams,
	);

	let createResultFromJson: ts.ExpressionStatement;
	if (returnRef !== null) {
		createResultFromJson = ts.createExpressionStatement(
			ts.createBinary(
				ts.createIdentifier('result'),
				ts.createToken(ts.SyntaxKind.EqualsToken),
				ts.createCall(
					ts.createPropertyAccess(ts.createIdentifier(returnRef.modelName), ts.createIdentifier('createFromJson')),
					[],
					[
						ts.createIdentifier('result'),
					],
				),
			),
		);
	}

	if (isSmartPatch) {
		statements.push(
			ts.createVariableStatement(
				[],
				ts.createVariableDeclarationList([
					ts.createVariableDeclaration(
						ts.createIdentifier('result'),
						undefined,
						undefined,
					),
				], ts.NodeFlags.Let),
			),
		);

		statements.push(
			ts.createIf(
				ts.createBinary(
					ts.createPropertyAccess(ts.createIdentifier('body'), ts.createIdentifier('length')),
					ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
					ts.createNumericLiteral('0'),
				),
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createBinary(
							ts.createIdentifier('result'),
							ts.createToken(ts.SyntaxKind.EqualsToken),
							ts.createIdentifier('resource'),
						),
					),
				], true),
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createBinary(
							ts.createIdentifier('result'),
							ts.createToken(ts.SyntaxKind.EqualsToken),
							ts.createAwait(fetch),
						),
					),
					...[createResultFromJson],
				], true),
			),
		);
	} else {
		statements.push(
			ts.createVariableStatement(
				[],
				ts.createVariableDeclarationList([
					ts.createVariableDeclaration(
						ts.createIdentifier('result'),
						undefined,
						ts.createAwait(fetch),
					),
				], ts.NodeFlags.Let),
			),
		);

		if (typeof createResultFromJson !== 'undefined') {
			statements.push(createResultFromJson);
		}
	}

	statements.push(
		ts.createExpressionStatement(
			ts.createAwait(
				ts.createCall(
					ts.createPropertyAccess(
						ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('logger')),
						ts.createIdentifier(endpoint.methodName),
					),
					[],
					[
						ts.createIdentifier('url'),
						ts.createObjectLiteral(group.ctorParameters.map((parameter) => {
							return ts.createPropertyAssignment(
								ts.createStringLiteral(parameter.name),
								ts.createElementAccess(ts.createThis(), ts.createStringLiteral(parameter.name)),
							);
						}), group.ctorParameters.length > 0),
						ts.createObjectLiteral(logData, logData.length > 0),
					],
				),
			),
		),
	);

	statements.push(
		ts.createReturn(
			ts.createIdentifier('result'),
		),
	);

	const returnType = returnRef === null ?
		ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword) :
		ts.createTypeReferenceNode(ts.createIdentifier(returnRef.modelName), []);

	const method = ts.createMethod(
		[],
		[
			ts.createToken(ts.SyntaxKind.PublicKeyword),
			ts.createToken(ts.SyntaxKind.AsyncKeyword),
		],
		undefined,
		ts.createIdentifier(endpoint.methodName),
		undefined,
		[],
		parameters,
		ts.createTypeReferenceNode(
			ts.createIdentifier('Promise'),
			[returnType],
		),
		ts.createBlock(statements, true),
	);

	return addJSDoc(method, comments);
}

function createExistsMethod(): ts.MethodDeclaration
{
	return ts.createMethod(
		[],
		[
			ts.createToken(ts.SyntaxKind.PublicKeyword),
			ts.createToken(ts.SyntaxKind.AsyncKeyword),
		],
		undefined,
		ts.createIdentifier('exists'),
		undefined,
		[],
		[],
		ts.createTypeReferenceNode(ts.createIdentifier('Promise'), [ts.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword)]),
		ts.createBlock([
			ts.createTry(
				ts.createBlock([
					ts.createExpressionStatement(
						ts.createAwait(
							ts.createCall(
								ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('read')),
								[],
								[],
							),
						),
					),
				], true),
				ts.createCatchClause(
					ts.createVariableDeclaration(
						ts.createIdentifier('e'),
						undefined,
						undefined,
					),
					ts.createBlock([
						ts.createIf(
							ts.createBinary(
								ts.createIdentifier('e'),
								ts.createToken(ts.SyntaxKind.InstanceOfKeyword),
								ts.createIdentifier('ApiResponseNotFoundError'),
							),
							ts.createReturn(ts.createFalse()),
							undefined,
						),
						ts.createThrow(ts.createIdentifier('e')),
					], true),
				),
				undefined,
			),
			ts.createReturn(ts.createTrue()),
		], true),
	);
}

function createPostOrMethod(doc: Document, group: EndpointGroup, orMethod: string): ts.MethodDeclaration
{
	const endpoint: Endpoint = group.endpoints.find((endpoint) => endpoint.method === 'post');
	if (typeof endpoint === 'undefined') {
		throw new Error('Should not happen');
	}

	const body = endpoint.getParameter(EndpointParameterPlace.Body, 'body');
	if (typeof body === 'undefined') {
		throw new Error('Should not happen');
	}

	const bodyRef = body.$ref;
	if (typeof bodyRef === 'undefined') {
		throw new Error('Should not happen');
	}

	const definition = doc.getDefinition(bodyRef);

	return ts.createMethod(
		[],
		[
			ts.createToken(ts.SyntaxKind.PublicKeyword),
			ts.createToken(ts.SyntaxKind.AsyncKeyword),
		],
		undefined,
		ts.createIdentifier('postOr' + firstUpper(orMethod)),
		undefined,
		[],
		[
			ts.createParameter(
				[],
				[],
				undefined,
				ts.createIdentifier('resource'),
				undefined,
				ts.createTypeReferenceNode(ts.createIdentifier(definition.modelName), []),
				undefined,
			),
		],
		ts.createTypeReferenceNode(ts.createIdentifier('Promise'), [
			ts.createTypeReferenceNode(ts.createIdentifier(definition.modelName), []),
		]),
		ts.createBlock([
			ts.createIf(
				ts.createBinary(
					ts.createBinary(
						ts.createTypeOf(
							ts.createPropertyAccess(ts.createIdentifier('resource'), ts.createIdentifier('metadata'))
						),
						ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
						ts.createStringLiteral('undefined'),
					),
					ts.createToken(ts.SyntaxKind.BarBarToken),
					ts.createBinary(
						ts.createTypeOf(
							ts.createPropertyAccess(
								ts.createPropertyAccess(ts.createIdentifier('resource'), ts.createIdentifier('metadata')),
								ts.createIdentifier('name')
							),
						),
						ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
						ts.createStringLiteral('undefined'),
					),
				),
				ts.createBlock([
					ts.createThrow(
						ts.createNew(
							ts.createIdentifier('Error'),
							[],
							[
								ts.createStringLiteral('PostOr' + firstUpper(orMethod) + ': resource is missing the metadata.name field'),
							],
						),
					),
				], true),
				undefined,
			),
			ts.createVariableStatement([], ts.createVariableDeclarationList([
				ts.createVariableDeclaration(
					ts.createIdentifier('byName'),
					undefined,
					ts.createCall(
						ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('byName')),
						[],
						[
							ts.createPropertyAccess(
								ts.createPropertyAccess(ts.createIdentifier('resource'), ts.createIdentifier('metadata')),
								ts.createIdentifier('name')
							),
						],
					),
				),
			], ts.NodeFlags.Const)),
			ts.createIf(
				ts.createAwait(
					ts.createCall(
						ts.createPropertyAccess(ts.createIdentifier('byName'), ts.createIdentifier('exists')),
						[],
						[],
					),
				),
				ts.createReturn(
					ts.createCall(
						ts.createPropertyAccess(ts.createIdentifier('byName'), ts.createIdentifier(orMethod)),
						[],
						[
							ts.createIdentifier('resource'),
						],
					),
				),
				undefined,
			),
			ts.createReturn(
				ts.createCall(
					ts.createPropertyAccess(ts.createThis(), ts.createIdentifier('post')),
					[],
					[
						ts.createIdentifier('resource'),
					],
				),
			)
		], true),
	)
}
