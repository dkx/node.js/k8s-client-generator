import {loadSpecification} from '../loader';
import {generateResources} from './resource';
import {generateStaticContent} from './static-generator';
import {generateEndpoints} from './endpoint';
import {generateClient} from './client-generator';
import {generateIndex} from './index-generator';
import {
	ArrayType,
	BooleanType,
	DateTimeType,
	IntOrStringType,
	Manager,
	NumberType, ObjectType,
	ReferenceType,
	StringType
} from '../openapi';


export async function generateApi(ver: string, outputDir: string): Promise<void>
{
	const doc = await loadSpecification(ver, outputDir);

	const typesManager = new Manager(doc, [
		new DateTimeType(),
		new StringType(),
		new NumberType(),
		new BooleanType(),
		new IntOrStringType(),
		new ReferenceType(),
		new ArrayType(),
		new ObjectType(),
	]);

	await generateStaticContent(doc);
	await generateResources(typesManager, doc);
	await generateEndpoints(doc);
	await generateClient(doc);
	await generateIndex(doc);
}
