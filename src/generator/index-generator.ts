import * as ts from 'typescript';
import * as fs from 'fs-extra';
import * as path from 'path';

import {Document} from '../openapi';
import {printTypescript} from '../utils';


export async function generateIndex(doc: Document): Promise<void>
{
	const exports: Array<string> = [
		'./resource',
		'./fetch',
		'./error',
		'./logging',
		'./client',
	];

	const filename = path.join(doc.dir, 'index.ts');
	const code = printTypescript(filename, [
		...exports.map((from): ts.ExportDeclaration => {
			return ts.createExportDeclaration(
				[],
				[],
				undefined,
				ts.createStringLiteral(from),
			);
		}),
	]);

	await fs.mkdirp(path.dirname(filename));
	await fs.writeFile(filename, code, {encoding: 'utf8'});
}
