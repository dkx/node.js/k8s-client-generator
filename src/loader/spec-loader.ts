import fetch from 'node-fetch';

import {Document, OpenApiDocument} from '../openapi';


export async function loadSpecification(ver: string, outputDir: string): Promise<Document>
{
	process.stdout.write('Downloading specification...');

	const url = `https://raw.githubusercontent.com/kubernetes/kubernetes/release-${ver}/api/openapi-spec/swagger.json`;
	const res = await fetch(url);
	const data: OpenApiDocument = await res.json();

	const doc = new Document(outputDir, data);

	console.log(' done');

	return doc;
}
