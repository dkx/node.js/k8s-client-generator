import * as relative from 'relative';
import * as R from 'ramda';


export function relativeImportPath(from: string, to: string): string
{
	let rel: string = relative(from, to).slice(0, -3);

	if (!R.startsWith('../', rel)) {
		rel = './' + rel;
	}

	if (R.endsWith('/index', rel)) {
		rel = rel.slice(0, -6);
	}

	return rel;
}
