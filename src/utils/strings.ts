export function firstUpper(str: string): string
{
	return str.charAt(0).toUpperCase() + str.slice(1);
}

export function firstLower(str: string): string
{
	return str.charAt(0).toLowerCase() + str.slice(1);
}

export function hyphensToCamelCase(str: string): string
{
	return str.replace(/-([a-z])/g, (g) => g[1].toUpperCase());
}

export function dotsToCamelCase(str: string): string
{
	return str.replace(/\.([a-z])/g, (g) => g[1].toUpperCase());
}

export function escapeRegExp(str: string): string
{
	return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
