import * as ts from 'typescript';

import {Definition} from '../openapi';


const ANY_TYPES: Array<string> = [
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSON',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSON',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceSubresourceStatus',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.CustomResourceSubresourceStatus',
	'io.k8s.apimachinery.pkg.apis.meta.v1.Fields',
	'io.k8s.apimachinery.pkg.apis.meta.v1.FieldsV1',
	'io.k8s.apimachinery.pkg.apis.meta.v1.Patch',
	'io.k8s.apimachinery.pkg.api.resource.Quantity',
	'io.k8s.apimachinery.pkg.runtime.RawExtension',
];


export function definitionShouldBeAny(name: string): boolean
{
	return ANY_TYPES.indexOf(name) >= 0;
}

export function definitionToTypeNode(definition: Definition): ts.TypeNode
{
	if (definitionShouldBeAny(definition.fullName)) {
		return ts.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword);
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrBool'
	) {
		return ts.createUnionTypeNode([
			ts.createTypeReferenceNode(ts.createIdentifier('JSONSchemaProps'), []),
			ts.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword),
		]);
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrStringArray' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrStringArray'
	) {
		return ts.createUnionTypeNode([
			ts.createTypeReferenceNode(ts.createIdentifier('JSONSchemaProps'), []),
			ts.createTypeReferenceNode(ts.createIdentifier('Array'), [
				ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
			])
		]);
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrArray'
	) {
		return ts.createUnionTypeNode([
			ts.createTypeReferenceNode(ts.createIdentifier('JSONSchemaProps'), []),
			ts.createTypeReferenceNode(ts.createIdentifier('Array'), [
				ts.createTypeReferenceNode(ts.createIdentifier('JSONSchemaProps'), []),
			])
		]);
	}

	return ts.createTypeReferenceNode(ts.createIdentifier(definition.modelName), []);
}

export function getDefinitionToJsonCallback(definition: Definition, accessor: ts.Expression): ts.Expression
{
	if (ANY_TYPES.indexOf(definition.fullName) >= 0) {
		return accessor;
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrBool'
	) {
		return ts.createConditional(
			ts.createBinary(
				ts.createTypeOf(accessor),
				ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
				ts.createStringLiteral('boolean'),
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, ts.createIdentifier('toJson')),
				[],
				[],
			),
		);
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrStringArray' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrStringArray'
	) {
		return ts.createConditional(
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('Array'), ts.createIdentifier('isArray')),
				[],
				[accessor],
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, ts.createIdentifier('toJson')),
				[],
				[],
			),
		);
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrArray'
	) {
		return ts.createConditional(
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('Array'), ts.createIdentifier('isArray')),
				[],
				[accessor],
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, ts.createIdentifier('map')),
				[],
				[
					ts.createArrowFunction(
						[],
						[],
						[
							ts.createParameter(
								[],
								[],
								undefined,
								ts.createIdentifier('item'),
							),
						],
						undefined,
						ts.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
						ts.createCall(
							ts.createPropertyAccess(ts.createIdentifier('item'), ts.createIdentifier('toJson')),
							[],
							[],
						),
					),
				],
			),
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, ts.createIdentifier('toJson')),
				[],
				[],
			),
		);
	}

	return ts.createCall(
		ts.createPropertyAccess(
			accessor,
			ts.createIdentifier('toJson'),
		),
		[],
		[],
	);
}

export function getDefinitionFromJsonCallback(definition: Definition, accessor: ts.Expression): ts.Expression
{
	if (ANY_TYPES.indexOf(definition.fullName) >= 0) {
		return accessor;
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrBool'
	) {
		return ts.createConditional(
			ts.createBinary(
				ts.createTypeOf(accessor),
				ts.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
				ts.createStringLiteral('boolean'),
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('JSONSchemaProps'), ts.createIdentifier('createFromJson')),
				[],
				[accessor],
			),
		);
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrStringArray' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrStringArray'
	) {
		return ts.createConditional(
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('Array'), ts.createIdentifier('isArray')),
				[],
				[accessor],
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('JSONSchemaProps'), ts.createIdentifier('createFromJson')),
				[],
				[accessor],
			),
		);
	}

	if (
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray' ||
		definition.fullName === 'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrArray'
	) {
		return ts.createConditional(
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('Array'), ts.createIdentifier('isArray')),
				[],
				[accessor],
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			ts.createCall(
				ts.createPropertyAccess(accessor, ts.createIdentifier('map')),
				[],
				[
					ts.createArrowFunction(
						[],
						[],
						[
							ts.createParameter(
								[],
								[],
								undefined,
								ts.createIdentifier('item'),
							),
						],
						undefined,
						ts.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
						ts.createCall(
							ts.createPropertyAccess(ts.createIdentifier('JSONSchemaProps'), ts.createIdentifier('createFromJson')),
							[],
							[ts.createIdentifier('item')],
						),
					),
				],
			),
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createCall(
				ts.createPropertyAccess(ts.createIdentifier('JSONSchemaProps'), ts.createIdentifier('createFromJson')),
				[],
				[accessor],
			),
		);
	}

	return ts.createCall(
		ts.createPropertyAccess(ts.createIdentifier(definition.modelName), ts.createIdentifier('createFromJson')),
		[],
		[accessor],
	);
}
