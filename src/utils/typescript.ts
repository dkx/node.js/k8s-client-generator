import * as ts from 'typescript';
import * as R from 'ramda';

import {relativeImportPath} from './path';


export declare interface NamedImport
{
	source: string,
	target: string,
	names: Array<string>,
}

export declare type NamedImports = Array<NamedImport>;


export function createNamedImports(imports: NamedImports): Array<ts.ImportDeclaration>
{
	return R.pipe(
		R.map((data: NamedImport): ts.ImportDeclaration => {
			return ts.createImportDeclaration(
				[],
				[],
				ts.createImportClause(
					undefined,
					ts.createNamedImports(data.names.map((name) => {
						return ts.createImportSpecifier(undefined, ts.createIdentifier(name));
					})),
				),
				ts.createStringLiteral(relativeImportPath(data.source, data.target)),
			);
		}),
	)(imports);
}

export function printTypescript(filename: string, statements: Array<ts.Statement>): string
{
	const file = ts.createSourceFile(filename, '', ts.ScriptTarget.Latest, false, ts.ScriptKind.TS);
	const printer = ts.createPrinter({
		newLine: ts.NewLineKind.LineFeed,
	});

	file.statements = ts.createNodeArray(statements);

	return printer.printNode(ts.EmitHint.Unspecified, file, file);
}

export function addJSDoc<T extends ts.Node>(node: T, content: Array<string>): T
{
	if (content.length === 0) {
		return node;
	}

	content = content
		.map((line) => {
			return line.replace(/\*\//g, '*' + String.fromCharCode(8203) + '/');
		})
		.map((line) => {
			return line.replace(/\n/gm, '\n * ');
		})
		.map((line) => {
			return ` * ${line}`;
		});

	content.unshift('*');
	content.push(' ');

	const text = content.join('\n');

	return ts.addSyntheticLeadingComment(node, ts.SyntaxKind.MultiLineCommentTrivia, text, true);
}

export function createStringConcatenation(nodes: Array<ts.Expression>): ts.Expression
{
	const optimized: Array<ts.Expression> = [];

	for (let i = 0; i < nodes.length; i++) {
		let node = nodes[i];

		if (ts.isStringLiteral(node) && node.text === '') {
			continue;
		}

		if (!ts.isStringLiteral(node) || optimized.length === 0) {
			optimized.push(node);
			continue;
		}

		let previous = optimized[optimized.length - 1];

		if (ts.isStringLiteral(node) && ts.isStringLiteral(previous)) {
			previous.text = previous.text + node.text;
		} else {
			optimized.push(node);
		}
	}

	if (optimized.length === 0) {
		throw new Error('Can not concatenate empty list of nodes');
	}

	if (optimized.length === 1) {
		return optimized[0];
	}

	return doCreateStringConcatenation(optimized);
}

function doCreateStringConcatenation(nodes: Array<ts.Expression>): ts.BinaryExpression
{
	const right = nodes.pop();
	const left = nodes.length === 1 ? nodes[0] : createStringConcatenation(nodes);

	return ts.createBinary(
		left,
		ts.createToken(ts.SyntaxKind.PlusToken),
		right,
	);
}
