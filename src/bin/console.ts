#!/usr/bin/env node

import * as path from 'path';
import * as yargs from 'yargs';

import {generateApi} from '../generator';

yargs
	.command('gen <ver> <out>', 'Generate client', (yargs) => {
		return yargs
			.positional('ver', {
				type: 'string',
				description: 'Kubernetes API version, eg. 1.16',
			})
			.positional('out', {
				type: 'string',
				description: 'Output directory',
			});
	}, async (argv) => {
		const outputDir: string = path.isAbsolute(argv.out) ?
			argv.out :
			path.join(process.cwd(), argv.out);

		await generateApi(argv.ver, outputDir);
	})
	.demandCommand()
	.argv;
