import * as ts from 'typescript';

import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export interface Type
{
	supports(property: SimpleProperty): boolean;

	createToJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression;

	createFromJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression;

	createFromOptionsExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression;

	getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode;

	getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode;
}
