import * as ts from 'typescript';

import {Type} from './type';
import {NativeType} from './native-type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export class StringType extends NativeType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return (
			property.apiType === 'string' ||
			(
				typeof property.$ref === 'string' &&
				property.$ref === 'io.k8s.apimachinery.pkg.api.resource.Quantity'
			)
		);
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return this.getDefinition(manager, doc, property);
	}
}
