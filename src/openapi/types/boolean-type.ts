import * as ts from 'typescript';

import {Type} from './type';
import {NativeType} from './native-type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export class BooleanType extends NativeType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return property.apiType === 'boolean';
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return ts.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return this.getDefinition(manager, doc, property);
	}
}
