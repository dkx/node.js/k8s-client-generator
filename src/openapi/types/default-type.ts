import * as ts from 'typescript';

import {Type} from './type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export class DefaultType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return true;
	}

	public createToJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}

	public createFromJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}

	public createFromOptionsExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		throw new Error(`Can not create typescript definition for type ${property.apiName}`);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return this.getDefinition(manager, doc, property);
	}
}
