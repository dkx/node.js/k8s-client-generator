import * as ts from 'typescript';

import {Type} from './type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export class ArrayType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return property.apiType === 'array';
	}

	public createToJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.createAccessorCall(
			accessor,
			manager.createToJsonExpression(property.items, ts.createIdentifier('item')),
		);
	}

	public createFromJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.createAccessorCall(
			accessor,
			manager.createFromJsonExpression(property.items, ts.createIdentifier('item')),
		);
	}

	public createFromOptionsExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.createAccessorCall(
			accessor,
			manager.createFromOptionsExpression(property.items, ts.createIdentifier('item')),
		);
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return ts.createArrayTypeNode(
			manager.getDefinition(property.items),
		);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return ts.createArrayTypeNode(
			manager.getOptionDefinition(property.items),
		);
	}

	private createAccessorCall(accessor: ts.Expression, inner: ts.Expression): ts.CallExpression
	{
		return ts.createCall(
			ts.createPropertyAccess(
				accessor,
				ts.createIdentifier('map'),
			),
			[],
			[
				ts.createArrowFunction(
					[],
					[],
					[
						ts.createParameter(
							[],
							[],
							undefined,
							ts.createIdentifier('item'),
						),
					],
					undefined,
					undefined,
					inner,
				),
			],
		);
	}
}
