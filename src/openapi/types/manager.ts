import * as ts from 'typescript';

import {Type} from './type';
import {DefaultType} from './default-type';
import {SimpleProperty} from '../simple-property';
import {Property} from '../property';
import {Document} from '../document';


export class Manager
{
	private defaultType = new DefaultType();

	constructor(
		private readonly document: Document,
		private readonly types: Array<Type> = [],
	) {}

	public createToJsonExpression(property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.findSupportedType(property).createToJsonExpression(this, this.document, property, accessor);
	}

	public createFromJsonExpression(property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.findSupportedType(property).createFromJsonExpression(this, this.document, property, accessor);
	}

	public createFromOptionsExpression(property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.findSupportedType(property).createFromOptionsExpression(this, this.document, property, accessor);
	}

	public getDefinition(property: SimpleProperty): ts.TypeNode
	{
		return this.findSupportedType(property).getDefinition(this, this.document, property);
	}

	public getOptionDefinition(property: SimpleProperty): ts.TypeNode
	{
		return this.findSupportedType(property).getOptionDefinition(this, this.document, property);
	}

	public createPropertyType(property: Property): ts.TypeNode
	{
		if (property.name === 'apiVersion' && property.definition.apiVersion.length > 1) {
			return ts.createUnionTypeNode(property.definition.apiVersion.map((version) => {
				return ts.createLiteralTypeNode(ts.createStringLiteral(version));
			}));
		}

		return this.getDefinition(property);
	}

	public createOptionType(property: Property): ts.TypeNode
	{
		if (property.name === 'apiVersion' && property.definition.apiVersion.length > 1) {
			return ts.createUnionTypeNode(property.definition.apiVersion.map((version) => {
				return ts.createLiteralTypeNode(ts.createStringLiteral(version));
			}));
		}

		return this.getOptionDefinition(property);
	}

	private findSupportedType(property: SimpleProperty): Type
	{
		for (let type of this.types) {
			if (type.supports(property)) {
				return type;
			}
		}

		return this.defaultType;
	}
}
