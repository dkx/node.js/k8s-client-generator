import * as ts from 'typescript';

import {Type} from './type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export abstract class NativeType implements Type
{
	public abstract getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode;

	public abstract getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode;

	public abstract supports(property: SimpleProperty): boolean;

	public createToJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}

	public createFromJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}

	public createFromOptionsExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}
}
