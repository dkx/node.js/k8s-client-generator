import * as ts from 'typescript';

import {Type} from './type';
import {Manager} from './manager';
import {NativeType} from './native-type';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export class IntOrStringType extends NativeType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return property.$ref === 'io.k8s.apimachinery.pkg.util.intstr.IntOrString';
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return ts.createUnionTypeNode([
			ts.createKeywordTypeNode(ts.SyntaxKind.NumberKeyword),
			ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
		]);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return this.getDefinition(manager, doc, property);
	}
}