import * as ts from 'typescript';

import {Type} from './type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';
import {
	definitionShouldBeAny,
	definitionToTypeNode,
	getDefinitionFromJsonCallback,
	getDefinitionToJsonCallback
} from '../../utils';


const JSON_TYPES: Array<string> = [
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSON',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaProps',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrStringArray',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSON',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaProps',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrBool',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrStringArray',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrArray',
];


export class ReferenceType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return typeof property.$ref === 'string';
	}

	public createToJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		const referenceName = property.$ref;
		if (typeof referenceName === 'undefined') {
			throw new Error(`Can not get reference from ${property.apiName}`);
		}

		const reference = doc.getDefinition(referenceName);
		return getDefinitionToJsonCallback(reference, accessor);
	}

	public createFromJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		const referenceName = property.$ref;
		if (typeof referenceName === 'undefined') {
			throw new Error(`Can not get reference from ${property.apiName}`);
		}

		const reference = doc.getDefinition(referenceName);
		return getDefinitionFromJsonCallback(reference, accessor);
	}

	public createFromOptionsExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		const referenceName = property.$ref;
		if (typeof referenceName === 'undefined') {
			throw new Error(`Can not get reference from ${property.apiName}`);
		}

		if (!this.canAcceptOptions(referenceName)) {
			return accessor;
		}

		const reference = doc.getDefinition(referenceName);

		return ts.createConditional(
			ts.createBinary(
				accessor,
				ts.createToken(ts.SyntaxKind.InstanceOfKeyword),
				ts.createIdentifier(reference.modelName),
			),
			ts.createToken(ts.SyntaxKind.QuestionToken),
			accessor,
			ts.createToken(ts.SyntaxKind.ColonToken),
			ts.createNew(
				ts.createIdentifier(reference.modelName),
				[],
				[accessor],
			),
		);
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		const referenceName = property.$ref;
		if (typeof referenceName === 'undefined') {
			throw new Error(`Can not get reference from ${property.apiName}`);
		}

		const reference = doc.getDefinition(referenceName);
		return definitionToTypeNode(reference);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		const referenceName = property.$ref;
		if (typeof referenceName === 'undefined') {
			throw new Error(`Can not get reference from ${property.apiName}`);
		}

		const reference = doc.getDefinition(referenceName);
		let type = definitionToTypeNode(reference);

		if (this.canAcceptOptions(referenceName) && ts.isTypeReferenceNode(type)) {
			type = ts.createUnionTypeNode([
				type,
				ts.createTypeReferenceNode(ts.createIdentifier(reference.optionsName), []),
			]);
		}

		return type;
	}

	private canAcceptOptions(name: string): boolean
	{
		if (definitionShouldBeAny(name)) {
			return false;
		}

		if (JSON_TYPES.indexOf(name) >= 0) {
			return false;
		}

		return true;
	}
}
