import * as ts from 'typescript';

import {Type} from './type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


export class ObjectType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return property.apiType === 'object';
	}

	public createToJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.createAccessorCall(
			accessor,
			manager.createToJsonExpression(property.additionalProperties, ts.createIdentifier('item')),
		);
	}

	public createFromJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.createAccessorCall(
			accessor,
			manager.createFromJsonExpression(property.additionalProperties, ts.createIdentifier('item')),
		);
	}

	public createFromOptionsExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return this.createAccessorCall(
			accessor,
			manager.createFromOptionsExpression(property.additionalProperties, ts.createIdentifier('item')),
		);
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return ts.createTypeLiteralNode([
			ts.createIndexSignature(
				[],
				[],
				[
					ts.createParameter(
						[],
						[],
						undefined,
						ts.createIdentifier('key'),
						undefined,
						ts.createKeywordTypeNode(ts.SyntaxKind.StringKeyword),
						undefined,
					),
				],
				manager.getDefinition(property.additionalProperties),
			),
		]);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return this.getDefinition(manager, doc, property);
	}

	private createAccessorCall(accessor: ts.Expression, inner: ts.Expression): ts.CallExpression
	{
		return ts.createCall(
			ts.createParen(
				ts.createArrowFunction(
					[],
					[],
					[
						ts.createParameter([], [], undefined, ts.createIdentifier('items')),
					],
					undefined,
					ts.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
					ts.createBlock([
						ts.createVariableStatement(
							[],
							ts.createVariableDeclarationList([
								ts.createVariableDeclaration(
									ts.createIdentifier('data'),
									undefined,
									ts.createObjectLiteral(),
								),
							], ts.NodeFlags.Const),
						),
						ts.createForIn(
							ts.createVariableDeclarationList([
								ts.createVariableDeclaration(ts.createIdentifier('key')),
							], ts.NodeFlags.Let),
							ts.createIdentifier('items'),
							ts.createBlock([
								ts.createIf(
									ts.createCall(
										ts.createPropertyAccess(ts.createIdentifier('items'), ts.createIdentifier('hasOwnProperty')),
										[],
										[ts.createIdentifier('key')],
									),
									ts.createBlock([
										ts.createVariableStatement([], ts.createVariableDeclarationList([
											ts.createVariableDeclaration(
												ts.createIdentifier('item'),
												undefined,
												ts.createElementAccess(ts.createIdentifier('items'), ts.createIdentifier('key')),
											),
										], ts.NodeFlags.Const)),
										ts.createExpressionStatement(ts.createBinary(
											ts.createElementAccess(ts.createIdentifier('data'), ts.createIdentifier('key')),
											ts.createToken(ts.SyntaxKind.EqualsToken),
											inner,
										)),
									], true),
								),
							], true),
						),
						ts.createReturn(ts.createIdentifier('data')),
					], true),
				),
			),
			[],
			[accessor],
		);
	}
}
