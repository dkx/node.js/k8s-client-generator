import * as ts from 'typescript';

import {Type} from './type';
import {Manager} from './manager';
import {SimpleProperty} from '../simple-property';
import {Document} from '../document';


const REF_NAMES: Array<string> = [
	'io.k8s.apimachinery.pkg.apis.meta.v1.Time',
	'io.k8s.apimachinery.pkg.apis.meta.v1.MicroTime',
];


export class DateTimeType implements Type
{
	public supports(property: SimpleProperty): boolean
	{
		return typeof property.$ref === 'string' && REF_NAMES.indexOf(property.$ref) >= 0;
	}

	public createToJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return ts.createCall(
			ts.createPropertyAccess(
				accessor,
				ts.createIdentifier('toISOString'),
			),
			[],
			[],
		);
	}

	public createFromJsonExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return ts.createNew(
			ts.createIdentifier('Date'),
			[],
			[accessor],
		);
	}

	public createFromOptionsExpression(manager: Manager, doc: Document, property: SimpleProperty, accessor: ts.Expression): ts.Expression
	{
		return accessor;
	}

	public getDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return ts.createTypeReferenceNode(ts.createIdentifier('Date'), []);
	}

	public getOptionDefinition(manager: Manager, doc: Document, property: SimpleProperty): ts.TypeNode
	{
		return this.getDefinition(manager, doc, property);
	}
}
