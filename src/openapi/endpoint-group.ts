import * as path from 'path';
import * as R from 'ramda';
import * as ts from 'typescript';

import {Endpoint} from './endpoint';
import {createStringConcatenation, firstUpper} from '../utils';
import {EndpointFactory} from './endpoint-factory';
import {EndpointParameter, EndpointParameterPlace} from './endpoint-parameter';


const URL_PARAM_PATTERN = /^{([a-z]+)}$/;


export class EndpointGroup
{
	public factories: Array<EndpointFactory> = [];

	public constructor(
		public readonly url: string,
		public readonly filename: string,
		public readonly isRoot: boolean,
		public readonly ctorParameters: Array<EndpointParameter>,
		public readonly endpoints: Array<Endpoint>,
	) {}

	public get className(): string
	{
		const name = path
			.basename(this.filename, '.ts')
			.replace(/\.([a-z])/g, (m) => m[1].toUpperCase())
			.replace(/-([a-z])/g, (m) => m[1].toUpperCase());

		return firstUpper(name);
	}

	public getDependenciesNames(): Array<string>
	{
		return R.pipe(
			R.reduce((dependencies, endpoint: Endpoint) => {
				return [...dependencies, ...endpoint.getDependenciesNames()];
			}, []),
			R.uniq,
		)(this.endpoints);
	}

	public findAssociatedDefinition(): string|undefined
	{
		const dependencies = R.pipe(
			R.map((endpoint: Endpoint): string|undefined => endpoint.findAssociatedDefinition()),
			R.filter((definition: string|undefined): boolean => typeof definition !== 'undefined'),
			R.uniq,
		)(this.endpoints);

		if (dependencies.length === 1) {
			return dependencies[0];
		}
	}

	public getUrlAsExpression(): ts.Expression
	{
		const parts = this.url
			.replace(/^\//, '')
			.split('/')
			.reduce((res, part) => [...res, '/', part], []);

		const nodes = parts.map((part) => {
			part = part.replace(/@/g, '/');

			const match = URL_PARAM_PATTERN.exec(part);
			if (match === null) {
				return ts.createStringLiteral(part)
			}

			return ts.createPropertyAccess(ts.createThis(), ts.createIdentifier(match[1]));
		});

		return createStringConcatenation(nodes);
	}

	public shouldCreateExistsShortcut(): boolean
	{
		const endpoint = this.endpoints.find((endpoint: Endpoint) => {
			if (endpoint.method !== 'get') {
				return false;
			}

			const name = endpoint.getParameter(EndpointParameterPlace.Path, 'name');
			return typeof name !== 'undefined';
		});

		return typeof endpoint !== 'undefined';
	}

	public shouldCreatePostOrShortcut(orMethod: string): boolean
	{
		const post = this.endpoints.find((endpoint: Endpoint) => endpoint.method === 'post');
		if (typeof post === 'undefined') {
			return false;
		}

		const byName = this.factories.find((factory: EndpointFactory) => factory.name === 'byName');
		if (typeof byName === 'undefined') {
			return false;
		}

		const method = byName.targetEndpointGroup.endpoints.find((endpoint: Endpoint) => endpoint.method === orMethod);
		return typeof method !== 'undefined';
	}

	public hasEndpoint(method: string): boolean
	{
		method = method.toLowerCase();
		const endpoint = this.endpoints.find((endpoint: Endpoint) => endpoint.method.toLowerCase() === method);
		return typeof endpoint !== 'undefined';
	}

	public merge(group: EndpointGroup): void
	{
		if (this.filename !== group.filename) {
			throw new Error(`Can not merge endpoint groups ${this.filename} with ${group.filename}`);
		}

		group.ctorParameters
			.filter((parameter) => {
				const existing = this.ctorParameters.find((existing) => existing.name === parameter.name && existing.place === parameter.place);
				return typeof existing === 'undefined';
			})
			.forEach((parameter) => {
				this.ctorParameters.push(parameter);
			});

		group.endpoints.forEach((endpoint) => {
			this.endpoints.push(endpoint);
		});

		group.factories
			.filter((factory) => {
				const existing = this.factories.find((existing) => existing.name === factory.name);
				return typeof existing === 'undefined';
			})
			.forEach((factory) => {
				this.factories.push(factory);
			});
	}
}
