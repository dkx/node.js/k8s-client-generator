import {OperationObject, ResponseObject} from 'openapi3-ts';
import * as R from 'ramda';

import {EndpointParameter, EndpointParameterPlace} from './endpoint-parameter';


export class Endpoint
{
	constructor(
		public readonly method: string,
		public readonly source: OperationObject,
		public readonly parameters: Array<EndpointParameter>,
	) {}

	public get methodName(): string
	{
		if (this.method === 'get') {
			return 'read';
		}

		return this.method;
	}


	public get description(): string|undefined
	{
		return this.source.description;
	}

	public getParameter(place: EndpointParameterPlace, name: string): EndpointParameter|undefined
	{
		return this.parameters.find((parameter) => {
			return parameter.name === name && parameter.place === place;
		});
	}

	public supportsDryRun(): boolean
	{
		const param = this.getParameter(EndpointParameterPlace.Query, 'dryRun');
		return typeof param !== 'undefined';
	}

	public getDependenciesNames(): Array<string>
	{
		const dependencies: Array<string> = [];

		const bodyParameter = this.getParameter(EndpointParameterPlace.Body, 'body');
		if (typeof bodyParameter !== 'undefined' && typeof bodyParameter.$ref !== 'undefined') {
			dependencies.push(bodyParameter.$ref);
		}

		const responseRef = this.getResponseRef(200);
		if (typeof responseRef !== 'undefined') {
			dependencies.push(responseRef);
		}

		return R.uniq(dependencies);
	}

	public findAssociatedDefinition(): string|undefined
	{
		if (this.method === 'delete' || this.method === 'patch') {
			return;
		}

		const dependencies = this.getDependenciesNames();

		if (this.method === 'get') {
			const list = dependencies.findIndex((dependency) => R.endsWith('List', dependency));
			if (list >= 0) {
				dependencies.splice(list, 1);
			}
		}

		if (dependencies.length === 1) {
			return dependencies[0];
		}
	}

	public getResponseRef(code: number): string|undefined
	{
		const str = code + '';
		for (let responseCode in this.source.responses) {
			if (responseCode === str) {
				const response: ResponseObject = this.source.responses[responseCode];
				const ref = typeof response.schema !== 'undefined' && typeof response.schema.$ref === 'string' && response.schema.$ref !== '' ?
					response.schema.$ref :
					undefined;

				return typeof ref === 'string' ? ref.substring(14) : undefined;
			}
		}
	}

	public isBodyRequired(): boolean
	{
		const body: any = this.source.parameters.find((param: any) => {
			return typeof param.in === 'string' && param.in === 'body';
		});

		return body.required === true;
	}
}
