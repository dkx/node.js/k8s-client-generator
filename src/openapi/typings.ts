import {OpenAPIObject} from 'openapi3-ts';


export declare interface OpenApiDocument extends OpenAPIObject
{
	definitions: OpenApiDefinitionsList,
}


export declare interface OpenApiDefinitionsList
{
	[name: string]: OpenApiDefinition,
}

export declare interface OpenApiDefinition
{
	type?: string,
	description?: string,
	properties?: OpenApiDefinitionPropertiesList,
	required?: Array<string>,
	'x-kubernetes-group-version-kind'?: Array<{group: string, kind: string, version: string}>,
}

export declare interface OpenApiDefinitionPropertiesList
{
	[name: string]: OpenApiDefinitionProperty,
}

export declare interface OpenApiDefinitionProperty
{
	$ref?: string,
	type?: string,
	format?: string,
	description?: string,
	items?: OpenApiDefinitionProperty,
	additionalProperties?: OpenApiDefinitionProperty,
}
