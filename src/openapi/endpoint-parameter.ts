export enum EndpointParameterPlace
{
	Query = 'query',
	Path = 'path',
	Body = 'body',
}


export class EndpointParameter
{
	constructor(
		private readonly source: any,
	) {}

	public get name(): string
	{
		return this.source.name;
	}

	public get place(): EndpointParameterPlace
	{
		return this.source.in;
	}

	public get description(): string|undefined
	{
		return this.source.description;
	}

	public get type(): string
	{
		return this.source.type;
	}

	public get $ref(): string|undefined
	{
		if (typeof this.source.schema === 'undefined' || typeof this.source.schema.$ref === 'undefined') {
			return;
		}

		return this.source.schema.$ref.substring(14);
	}
}
