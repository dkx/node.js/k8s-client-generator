import * as R from 'ramda';
import * as path from 'path';
import {PathItemObject} from 'openapi3-ts/src/model/OpenApi';
import {OperationObject} from 'openapi3-ts';

import {Definition} from './definition';
import {EndpointGroup} from './endpoint-group';
import {Endpoint} from './endpoint';
import {OpenApiDefinition, OpenApiDocument} from './typings';
import {EndpointParameter, EndpointParameterPlace} from './endpoint-parameter';
import {EndpointFactory} from './endpoint-factory';
import {dotsToCamelCase, firstUpper} from '../utils';


const PATH_PARAMETER_REGEX = /^{([a-z]+)}$/;

const ALLOWED_METHODS: Array<string> = [
	'get', 'put', 'post', 'delete', 'patch',
];


export class Document
{
	public readonly definitions: Array<Definition>;

	public readonly endpointGroups: Array<EndpointGroup>;

	constructor(
		public readonly dir: string,
		source: OpenApiDocument,
	) {
		const resourceDir = this.resourceDir;

		this.definitions = R.pipe(
			R.mapObjIndexed((definition: OpenApiDefinition, name: string): Definition => {
				return new Definition(resourceDir, name, definition);
			}),
			R.values,
		)(source.definitions || {});

		this.endpointGroups = this.createEndpointGroups(source.paths || {});
	}

	public get resourceDir(): string
	{
		return path.join(this.dir, 'resource');
	}

	public get endpointDir(): string
	{
		return path.join(this.dir, 'endpoint');
	}

	public getDefinition(fullName: string): Definition
	{
		const definition = this.definitions.find((definition) => {
			return definition.fullName === fullName;
		});

		if (typeof definition === 'undefined') {
			throw new Error(`Definition ${fullName} does not exists`);
		}

		return definition;
	}

	public getRootApis(): Array<EndpointGroup>
	{
		return this.endpointGroups.filter((group) => {
			return group.isRoot && group.ctorParameters.length === 0;
		});
	}

	private createEndpointGroups(paths: PathItemObject): Array<EndpointGroup>
	{
		const groups: Array<EndpointGroup> = [];
		let parentGroups: Array<EndpointGroup> = [];

		R.forEachObjIndexed((group: PathItemObject, url: string) => {
			const data = this.createEndpointGroup(url, group);

			groups.push(data.group);
			parentGroups = [...parentGroups, ...data.parents];
		}, paths);

		parentGroups.forEach((group) => {
			const existing: EndpointGroup = groups.find((existing) => existing.filename === group.filename);
			if (typeof existing === 'undefined') {
				groups.push(group);
			} else {
				existing.merge(group);
			}
		});

		groups.forEach((group) => {
			group.factories = group.factories.map((factory) => {
				const parent = groups.find((parent) => parent.filename === factory.parentEndpointGroup.filename);
				const target = groups.find((target) => target.filename === factory.targetEndpointGroup.filename);

				if (typeof parent === 'undefined' || typeof target === 'undefined') {
					throw new Error('Should not happen');
				}

				return new EndpointFactory(
					factory.name,
					parent,
					target,
				);
			});
		});

		return groups;
	}

	private createEndpointGroup(url: string, group: PathItemObject): {group: EndpointGroup, parents: Array<EndpointGroup>}
	{
		const fileParts: Array<string> = [];
		const parents: Array<EndpointGroup> = [];
		const ctorParameters: Array<EndpointParameter> = [];

		let methodParameter: EndpointParameter;
		let previousGroup: EndpointGroup;

		url
			.split('/')
			.filter((urlPart) => urlPart !== '')
			.forEach((urlPart, i) => {
				const parameterMatch = PATH_PARAMETER_REGEX.exec(urlPart);

				if (parameterMatch === null) {
					fileParts.push(urlPart);
				} else {
					const parameter = group.parameters.find((parameter: any) => {
						return parameter.name === parameterMatch[1] && parameter.in === EndpointParameterPlace.Path;
					});

					if (typeof parameter === 'undefined') {
						throw new Error(`Parameter ${parameterMatch[1]} does not exists in path`);
					}

					const endpointParameter = new EndpointParameter(parameter);
					ctorParameters.push(endpointParameter);
					methodParameter = endpointParameter;
				}

				let filename = fileParts.join('/') + '-api.ts';
				let factoryName = dotsToCamelCase(fileParts[fileParts.length - 1]);

				if (typeof methodParameter !== 'undefined') {
					factoryName = 'by' + firstUpper(methodParameter.name);
					filename = fileParts.join('/') + '-by-' + methodParameter.name + '-api.ts';

					methodParameter = undefined;
				}

				const isRoot = i === 0;

				const parentGroup = new EndpointGroup(
					'',
					path.join(this.endpointDir, filename),
					isRoot,
					[...ctorParameters],
					[],
				);

				if (typeof previousGroup !== 'undefined') {
					previousGroup.factories.push(new EndpointFactory(factoryName, previousGroup, parentGroup));
				}

				parents.push(parentGroup);
				previousGroup = parentGroup;
			});

		const last = parents.pop();

		const endpoints = R.pipe(
			R.pickBy((endpoint: OperationObject, method: string) => {
				return ALLOWED_METHODS.indexOf(method) >= 0;
			}),
			R.mapObjIndexed((endpoint: OperationObject, method: string) => {
				const parameters = [
					...(group.parameters || []),
					...(endpoint.parameters || []),
				].map((parameter: any) => {
					return new EndpointParameter(parameter);
				});

				return new Endpoint(method, endpoint, parameters);
			}),
			R.values,
		)(group);

		return {
			parents,
			group: new EndpointGroup(
				url,
				last.filename,
				last.isRoot,
				last.ctorParameters,
				endpoints,
			),
		};
	}
}
