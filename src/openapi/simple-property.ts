import * as R from 'ramda';

import {OpenApiDefinitionProperty} from './typings';


export class SimpleProperty
{
	public readonly items: SimpleProperty|undefined;

	public readonly additionalProperties: SimpleProperty|undefined;

	constructor(
		public readonly apiName: string,
		protected readonly source: OpenApiDefinitionProperty,
	) {
		if (typeof this.source.items !== 'undefined') {
			this.items = new SimpleProperty(`${this.apiName}.items`, this.source.items);
		}

		if (typeof this.source.additionalProperties !== 'undefined') {
			this.additionalProperties = new SimpleProperty(`${this.apiName}.additionalProperties`, this.source.additionalProperties);
		}
	}

	public get apiType(): string|undefined
	{
		return this.source.type;
	}

	public get description(): string|undefined
	{
		return this.source.description;
	}

	public get $ref(): string|undefined
	{
		if (typeof this.source.$ref === 'string' && this.source.$ref !== '') {
			return this.source.$ref.substring(14);
		}
	}

	public getDependenciesNames(): Array<string>
	{
		let deps: Array<string> = [];

		const ref = this.$ref;
		if (typeof ref !== 'undefined') {
			deps.push(ref);
		}

		if (typeof this.items !== 'undefined') {
			deps = [...deps, ...this.items.getDependenciesNames()];
		}

		if (typeof this.additionalProperties !== 'undefined') {
			deps = [...deps, ...this.additionalProperties.getDependenciesNames()];
		}

		return R.uniq(deps);
	}
}
