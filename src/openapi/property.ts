import * as ts from 'typescript';

import {OpenApiDefinitionProperty} from './typings';
import {Definition} from './definition';
import {SimpleProperty} from './simple-property';


export class Property extends SimpleProperty
{
	constructor(
		public readonly definition: Definition,
		apiName: string,
		source: OpenApiDefinitionProperty,
		public required: boolean,
	) {
		super(apiName, source);
	}

	public get name(): string
	{
		return this.apiName.replace(/-/g, '_');
	}

	public get value(): ts.Expression|undefined
	{
		if (this.name === 'apiVersion' && this.definition.apiVersion.length === 1) {
			return ts.createStringLiteral(this.definition.apiVersion[0]);
		}

		if (this.name === 'kind' && this.definition.kind.length === 1) {
			return ts.createStringLiteral(this.definition.kind[0]);
		}
	}

	public get inOptions(): boolean
	{
		if (this.name === 'apiVersion' && this.definition.apiVersion.length === 1) {
			return false;
		}

		if (this.name === 'kind' && this.definition.kind.length === 1) {
			return false;
		}

		return true;
	}
}
