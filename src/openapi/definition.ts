import * as path from 'path';
import * as R from 'ramda';

import {Property} from './property';
import {OpenApiDefinition, OpenApiDefinitionProperty} from './typings';
import {SimpleProperty} from './simple-property';


const SKIP_NAMES: Array<string> = [
	'io.k8s.apimachinery.pkg.util.intstr.IntOrString',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSON',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrStringArray',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceSubresourceStatus',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSON',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrArray',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrBool',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.JSONSchemaPropsOrStringArray',
	'io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1.CustomResourceSubresourceStatus',
	'io.k8s.apimachinery.pkg.apis.meta.v1.MicroTime',
	'io.k8s.apimachinery.pkg.apis.meta.v1.Time',
	'io.k8s.apimachinery.pkg.apis.meta.v1.Fields',
	'io.k8s.apimachinery.pkg.apis.meta.v1.FieldsV1',
	'io.k8s.apimachinery.pkg.apis.meta.v1.Patch',
	'io.k8s.apimachinery.pkg.api.resource.Quantity',
	'io.k8s.apimachinery.pkg.runtime.RawExtension',
];


export class Definition
{
	public readonly apiVersion: Array<string>;

	public readonly kind: Array<string>;

	public readonly properties: Array<Property>;

	constructor(
		private readonly dir: string,
		public readonly fullName: string,
		private readonly source: OpenApiDefinition,
	) {
		if (!fullName.startsWith('io.k8s.')) {
			throw new Error(`Unsupported resource "${fullName}"`);
		}

		this.apiVersion = this.extractApiVersion();
		this.kind = this.extractKind();
		this.properties = this.mapProperties();
	}

	public get name(): string
	{
		return this.fullName.substring(7);
	}

	public get filename(): string
	{
		return path.join(this.dir, ...this.name.split('.')) + '.ts';
	}

	public get modelName(): string
	{
		return this.name.substring(this.name.lastIndexOf('.') + 1);
	}

	public get optionsName(): string
	{
		return this.name.substring(this.name.lastIndexOf('.') + 1) + 'Options';
	}

	public get description(): string|undefined
	{
		return this.source.description;
	}

	public get shouldBeSkipped(): boolean
	{
		return SKIP_NAMES.indexOf(this.fullName) >= 0;
	}

	public get sortedProperties(): Array<Property>
	{
		return this.properties.sort((a, b) => {
			if (a.required && !b.required) {
				return -1;
			}

			if (!a.required && b.required) {
				return 1;
			}

			const aName = a.name.toLowerCase();
			const bName = b.name.toLowerCase();

			if (aName < bName) {
				return -1;
			}

			if (aName > bName) {
				return 1;
			}

			return 0;
		});
	}

	public isDeprecated(): boolean
	{
		const description = this.description;
		if (typeof description === 'string' && description.toLowerCase().startsWith('deprecated')) {
			return true;
		}

		return false;
	}

	public getDependenciesNames(): Array<string>
	{
		const deps: Array<string> = this.properties
			.reduce((dependencies: Array<string>, property: SimpleProperty) => {
				return [...dependencies, ...property.getDependenciesNames()];
			}, [])
			.filter((dependency) => {
				return dependency !== this.fullName;
			});

		return R.uniq(deps);
	}

	private mapProperties(): Array<Property>
	{
		const required = this.source.required || [];

		if (Array.isArray(this.source['x-kubernetes-group-version-kind'])) {
			required.push('apiVersion');
			required.push('kind');
		}

		return R.pipe(
			R.mapObjIndexed((property: OpenApiDefinitionProperty, name: string) => {
				return new Property(this, name, property, required.indexOf(name) >= 0);
			}),
			R.values,
		)(this.source.properties || {});
	}

	private extractApiVersion(): Array<string>
	{
		if (Array.isArray(this.source['x-kubernetes-group-version-kind'])) {
			const apiVersion = this.source['x-kubernetes-group-version-kind'].map((v) => {
				return v.group === '' ?
					v.version :
					v.group + '/' + v.version;
			});

			return R.uniq(apiVersion);
		}

		return [];
	}

	private extractKind(): Array<string>
	{
		if (Array.isArray(this.source['x-kubernetes-group-version-kind'])) {
			const kind = this.source['x-kubernetes-group-version-kind'].map((v) => {
				return v.kind;
			});

			return R.uniq(kind);
		}

		return [];
	}
}
