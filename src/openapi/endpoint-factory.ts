import {EndpointGroup} from './endpoint-group';
import {EndpointParameter} from './endpoint-parameter';


export class EndpointFactory
{
	constructor(
		public readonly name: string,
		public readonly parentEndpointGroup: EndpointGroup,
		public readonly targetEndpointGroup: EndpointGroup,
	) {}

	public get parentParameters(): Array<EndpointParameter>
	{
		return this.parentEndpointGroup.ctorParameters.filter((parameter) => {
			const inTarget = this.targetEndpointGroup.ctorParameters.find((inTarget) => inTarget.name === parameter.name);
			return typeof inTarget !== 'undefined';
		});
	}

	public get methodParameters(): Array<EndpointParameter>
	{
		return this.targetEndpointGroup.ctorParameters.filter((parameter) => {
			const fromParent = this.parentEndpointGroup.ctorParameters.find((fromParent) => fromParent.name === parameter.name);
			return typeof fromParent === 'undefined';
		});
	}
}
