import {expect} from 'chai';

import {
	firstLower,
	firstUpper,
} from '../../src/utils';


describe('#utils', () => {

	describe('firstUpper()', () => {

		it('should uppercase first letter', () => {
			expect(firstUpper('hello')).to.be.equal('Hello');
		});

	});

	describe('firstLower()', () => {

		it('should lowercase first letter', () => {
			expect(firstLower('HELLO')).to.be.equal('hELLO');
		});

	});

});
