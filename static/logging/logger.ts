import {Resource} from '../resource';


export declare interface LoggedData
{
	resource?: Resource,
	result?: any,
	body?: any,
	definition?: {
		fullName: string,
		className: string,
	},
	endpoint?: {
		method: string,
		urlPattern: string,
	},
}

export declare interface UrlParameters
{
	[name: string]: string,
}


export interface Logger
{
	read(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	post(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	put(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	patch(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;

	delete(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>;
}
