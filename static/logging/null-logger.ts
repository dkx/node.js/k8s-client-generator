import {LoggedData, Logger, UrlParameters} from './logger';


export class NullLogger implements Logger
{
	public async read(url: string, parameters: UrlParameters, data: LoggedData): Promise<void> {}

	public async post(url: string, parameters: UrlParameters, data: LoggedData): Promise<void> {}

	public async put(url: string, parameters: UrlParameters, data: LoggedData): Promise<void> {}

	public async patch(url: string, parameters: UrlParameters, data: LoggedData): Promise<void> {}

	public async delete(url: string, parameters: UrlParameters, data: LoggedData): Promise<void> {}
}
