import {LoggedData, Logger, UrlParameters} from './logger';


export class StdOutLogger implements Logger
{
	public async read(url: string, parameters: UrlParameters, data: LoggedData): Promise<void> {}

	public post(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		return this.logCall('Created', url, data);
	}

	public put(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		return this.logCall('Replaced', url, data);
	}

	public delete(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		return this.logCall('Deleted', url, data);
	}

	public async patch(url: string, parameters: UrlParameters, data: LoggedData): Promise<void>
	{
		const prefix = Array.isArray(data.body) && data.body.length === 0 ?
			'Unchanged' :
			'Updated';

		await this.logCall(prefix, url, data);

		if (Array.isArray(data.body)) {
			data.body.forEach((operation) => {
				console.log(`  - ${operation.op} ${operation.path}`);
			});
		}
	}

	private async logCall(prefix: string, url: string, data: LoggedData): Promise<void>
	{
		console.log(this.createMessage(prefix, url, data));
	}

	private createMessage(prefix: string, url: string, data: LoggedData): string
	{
		let suffix = url;

		if (typeof data.resource !== 'undefined') {
			const type: Array<string> = [];
			const name: Array<string> = [];

			if (typeof data.resource.apiVersion !== 'undefined') {
				type.push(data.resource.apiVersion);
			}

			if (typeof data.resource.kind !== 'undefined') {
				type.push(data.resource.kind);
			}

			if (typeof data.resource.metadata.namespace !== 'undefined') {
				name.push(data.resource.metadata.namespace);
			}

			if (typeof data.resource.metadata.name !== 'undefined') {
				name.push(data.resource.metadata.name);
			}

			const parts: Array<string> = [];

			if (type.length > 0) {
				parts.push(type.join('/'));
			}

			if (name.length > 0) {
				parts.push(name.join('/'));
			}

			if (parts.length > 0) {
				suffix = parts.join(' ');
			}
		}

		return `${prefix} ${suffix}`
	}
}
