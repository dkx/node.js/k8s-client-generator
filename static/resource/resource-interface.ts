import {ObjectMeta} from './apimachinery/pkg/apis/meta/v1/ObjectMeta';


export interface Resource
{
	apiVersion?: string,
	kind?: string,
	metadata?: ObjectMeta,
	toJson(): any;
}
