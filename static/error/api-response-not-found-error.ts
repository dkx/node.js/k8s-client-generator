import {Response} from 'node-fetch';

import {ApiResponseError} from './api-response-error';


export class ApiResponseNotFoundError extends ApiResponseError
{
	constructor(response: Response, message: string)
	{
		super(response, message);
		Object.setPrototypeOf(this, ApiResponseNotFoundError.prototype);
	}
}
