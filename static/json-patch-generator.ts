import {compare} from 'fast-json-patch';

import {Resource} from './resource';


const IGNORE_OPERATIONS: Array<{op: string, path: string}> = [
	{op: 'remove', path: '/status'},
	{op: 'remove', path: '/spec'},
	{op: 'remove', path: '/metadata/uid'},
	{op: 'remove', path: '/metadata/selfLink'},
	{op: 'remove', path: '/metadata/resourceVersion'},
	{op: 'remove', path: '/metadata/creationTimestamp'},
];


export class JsonPatchGenerator
{
	public createForResource(from: Resource, to: Resource): any
	{
		return compare(from.toJson(), to.toJson())
			.filter((patch) => {
				const ignore = IGNORE_OPERATIONS.find((ignore) => ignore.op === patch.op && ignore.path === patch.path);
				return typeof ignore === 'undefined';
			});
	}
}
